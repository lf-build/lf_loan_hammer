/*
// Require Sys and FileSystem
var fs = require('fs');

// Require package
//var validate = require('json-schema/lib/validate').validate;

// Load data file
fs.readFile('user.json', function(err, data) {
    if(err) throw err;

    var validation = validate(data);
    if(validation == null || validation.length == 0){
        // sucess
        console.log("\n success!");
    }
    else{
        // error
        console.log("\n"+ validation);
    }
});
*/
module.exports =
{
    validate : function (data){
        try {
            if(data == null || data == undefined || data.length == 0){
                throw "Request body is empty";
            }

            // Parse as JSON
            var json = JSON.parse(data)[0];

            //console.log(json);

            if(isNullOrWhiteSpace(json.referenceNumber))
                throw "referenceNumber is mandatory";

            if(isNullOrWhiteSpace(json.fundingSource))
                throw "fundingSource is mandatory";

            if(isNullOrWhiteSpace(json.purpose))
                throw "purpose is mandatory";

            if (isNullOrUndefined(json.investor))
              throw "investor is mandatory";

            if (isNullOrWhiteSpace(json.investor.id))
              throw "investor id is mandatory";

            if (isNullOrWhiteSpace(json.investor.loanPurchaseDate))
              throw "investor's loanPurchaseDate is mandatory";

            if (isNullOrUndefined(json.scores))
              throw "scores is mandatory";

            if (!Array.isArray(json.scores))
              throw "scores should be an array";

            if (json.scores.length === 0)
              throw "There should be at least one score";

            for(var i=0; i<json.scores.length; i++) {
              var score = json.scores[i];
              if (isNullOrWhiteSpace(score.name))
                throw "score name is mandatory";
              if (isNullOrWhiteSpace(score.initialDate))
                throw "initialDate is mandatory";
              if (isNullOrWhiteSpace(score.initialScore))
                throw "initialScore is mandatory";
            }

            if (isNullOrWhiteSpace(json.grade))
              throw "grade is mandatory";

            if (isNullOrWhiteSpace(json.monthlyIncome))
              throw "monthlyIncome is mandatory";

            if (isNullOrWhiteSpace(json.homeOwnership))
              throw "homeOwnership is mandatory";

            // terms.loanAmount
            if(json.terms.loanAmount == null || isNaN(json.terms.loanAmount) || json.terms.loanAmount <= 0.0)
                throw "loanAmount must be greater than zero";

            if(json.terms.term == null || isNaN(json.terms.term) || json.terms.term <= 0)
                throw "Loan term must be greater than zero";

            if (json.terms.rate == null || isNaN(json.terms.rate) || json.terms.rate <= 0.0 || json.terms.rate > 100.0)
                throw "Loan rate must be in the interval (0, 100]";

            if (json.terms.rateType == null || json.terms.rateType.length == 0 ||
                json.terms.rateType == undefined || json.terms.rateType.toLowerCase() == "Undefined".toLowerCase())
                throw "Rate type is mandatory";

            if (json.terms.fees == null)
                throw "Fee list cannot be null";

            if (isNullOrWhiteSpace(json.paymentMethod) || json.paymentMethod.toLowerCase() == "undefined")
                throw "paymentMethod is mandatory";

            if (json.paymentMethod.toLowerCase() == "ach")
                validateBankInfo(json.bankInfo);

            validateBorrowers(json.borrowers);
        }
        catch(error) {
            return error;
        }
    }
}

function isNullOrUndefined(field) {
    return typeof field === 'undefined' || field == null;
}
function isNullOrWhiteSpace(field) {
    return field == null || field.length == 0 || field == " ";
}

function validateBankInfo(bankInfo){
    if (bankInfo == null)
        throw "Bank info is mandatory when payment method is ACH";

    if (isNullOrWhiteSpace(bankInfo.routingNumber))
        throw "Bank routing number is mandatory when payment method is ACH";

    if (isNullOrWhiteSpace(bankInfo.accountNumber))
        throw "Bank account number is mandatory when payment method is ACH";

    if (isNullOrWhiteSpace(bankInfo.accountType) ||
        bankInfo.accountType == undefined ||
        bankInfo.accountType.toLowerCase() == "Undefined".toLocaleLowerCase())
        throw "Bank account type is mandatory when payment method is ACH";
}

function validateBorrowers(borrowers) {
    if (borrowers == null)
        throw "Borrower list cannot be null";

    if (borrowers.length == 0)
        throw "Borrower list cannot be empty";

    for (var i = 0; i < borrowers.length; i++) {
        validateBorrower(borrowers[i]);
    }

    var hasIsPrimary = false;
    var countPrimary = 0;
    for (var i = 0; i < borrowers.length; i++) {
        if(borrowers[i].isPrimary){
            hasIsPrimary = true;
            countPrimary++;
        }
    }

    if (hasIsPrimary == false)
        throw "At least one borrower must be primary";

    if (countPrimary > 1)
        throw "At most one borrower must be primary";
}

function validateBorrower(borrower)
{
    if (borrower == null)
        throw "Borrower cannot be null";

    if (isNullOrWhiteSpace(borrower.referenceNumber))
        throw "Borrower reference number name is mandatory";

    if (isNullOrWhiteSpace(borrower.firstName))
        throw "Borrower first name is mandatory";

    if (isNullOrWhiteSpace(borrower.lastName))
        throw "Borrower last name is mandatory";

    if (isNullOrWhiteSpace(borrower.ssn))
        throw "Borrower SSN is mandatory";

    if (isNullOrWhiteSpace(borrower.email))
        throw "Borrower email is mandatory";

    validateAddresses(borrower.addresses);

    validatePhones(borrower.phones);
}

function validateAddresses(addresses)
{
    if (addresses == null)
        throw "Borrower address list cannot be null";

    if (addresses.length == 0)
        throw "Borrower address list cannot be empty";

    for (var i = 0; i < addresses.length; i++) {
        validateAddress(addresses[i]);
    }

    var hasIsPrimary = false;
    var countPrimary = 0;
    for (var i = 0; i < addresses.length; i++) {
        if(addresses[i].isPrimary){
            hasIsPrimary = true;
            countPrimary++;
        }
    }

    if (hasIsPrimary == false)
        throw "Borrower must have at least one primary address";

    if (countPrimary > 1)
        throw "Borrower cannot have more than two primary addresses";
}

function validateAddress(address)
{
    if (address == null)
        throw "Borrower address cannot be null";

    if (isNullOrWhiteSpace(address.line1))
        throw "Invalid borrower address: line 1 is mandatory";

    if (isNullOrWhiteSpace(address.city))
        throw "Invalid borrower address: city is mandatory";

    if (isNullOrWhiteSpace(address.state))
        throw "Invalid borrower address: state is mandatory";

    if (isNullOrWhiteSpace(address.zipCode))
        throw "Invalid borrower address: zip code is mandatory";
}

function validatePhones(phones){
    if (phones == null)
        throw "Borrower phone list cannot be null";

    if (phones.length == 0)
        throw "Borrower phone list cannot be empty";

    for (var i = 0; i < phones.length; i++) {
        validatePhone(phones[i]);
    }

    var hasIsPrimary = false;
    var countPrimary = 0;
    for (var i = 0; i < phones.length; i++) {
        if(phones[i].isPrimary){
            hasIsPrimary = true;
            countPrimary++;
        }
    }

    if (hasIsPrimary == false)
        throw "Borrower must have at least one primary phone";

    if (countPrimary > 1)
        throw "Borrower must have at most one primary phone";
}

function validatePhone(phone)
{
    if (phone == null)
        throw "Borrower phone cannot be null";

    if (isNullOrWhiteSpace(phone.number))
        throw "Borrower phone number is mandatory";
}
