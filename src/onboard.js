var http = require("http");
var querystring = require("querystring");
var util = require("util");
var port = 7000;
var check = require("./validate.js");

var server = http.createServer(function (request, response) {
    if (request.method == 'POST') {

        var url = request.url;
        if(url != '/onboard'){
            console.log("[400] " + request.method + " to " + request.url);
            response.writeHead(400, "Not found", {"Content-Type": "application/json; charset=utf-8"});
            response.write('"{"code":400,"message":"Not found"}"');
            response.end();
        }
        else{
            console.log("[200] " + request.method + " to " + request.url);
            var fullBody = '';

            request.on('data', function(chunk) {
            // append the current chunk of data to the fullBody variable
            fullBody += chunk.toString();
            });

            request.on('end', function() {

            // request ended -> do something with the data
            response.writeHead(200, "OK", {'Content-Type': 'text/html'});

            // parse the received body data
            //var decodedBody = querystring.parse(fullBody);

            var resultCheck = check.validate(fullBody);
            if(resultCheck == null || resultCheck.length == 0){
                // sucess
                console.log("\n success!");
                response.writeHead(204, "Success", {"Content-Type": "application/json; charset=utf-8"});
                response.write('');
                //response.write("<html><head><title>Success</title></head><body><h2>Success.</h2></body></html>");
            }
            else{
                // error
                console.log("\n"+ resultCheck);
                response.writeHead(400, "Bad Request", {"Content-Type": "application/json; charset=utf-8"});
                response.write('"{"code":400,"message":"' + resultCheck + '"}"');
            }

            // output the decoded data to the HTTP response
            //response.write('<html><head><title>Post data</title></head><body><pre>');
            //response.write(util.inspect(decodedBody));
            //response.write('</pre></body></html>');

            response.end();
            });
        }
    } else {
        console.log("[405] " + request.method + " to " + request.url);
        response.writeHead(405, "Method not supported", {'Content-Type': 'text/html'});
        response.end('<html><head><title>405 - Method not supported</title></head><body><h1>Method not supported.</h1></body></html>');
    }
});

server.listen(port);
console.log("Server listening on ( " + port + " )");
