FROM node

# use our prepared Raspberry Pi compatible Docker base image with Node.js
# FROM hypriot/rpi-node:0.12.0

# make the src folder available in the docker image
ADD ./src/ /app
WORKDIR /app

# ADD ./src /app
#WORKDIR /app/onboarding-test

ENV MONO_THREADS_PER_CPU 2000

# install the dependencies from the package.json file
RUN npm install

# Bundle app source
# COPY src/app

# make port 80 available outside of the image
EXPOSE 7000

# start node with the index.js file of our hello-world application
# CMD ["node", "index.js"]
CMD [ "node", "onboard.js" ]